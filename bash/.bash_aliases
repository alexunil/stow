###colorisiern wenn möglich
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    #alias ls='exa'
    alias nicecat='batcat'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

##Nur noch Vi (vim!)
alias nano='vi'
alias snano='sudo vi'
alias svi='sudo vi'
## Typische aliase
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
## Docker abkürzungen
alias dcls='docker container ls'
alias dce='docker exec -it'
alias dcr='docker container rm'
alias dils='docker image ls'
alias catcert='openssl x509 -text -in'
## Weil golocal keine curl mag (scriptkiddies)
alias curl='curl -H "User-Agent: wget"'
###Meine VPN-Verbindung (laptop)
alias vpn='sudo openvpn --config /home/alex/bin/gehreral@dackel.netzarbeit.net.ovpn'
alias vpn='~/bin/vpn_connect.sh'
alias dns='sudo /usr/bin/cp /home/alex/bin/resolv.conf /etc/resolv.conf'
###witzig
alias please='sudo'
#Notes
alias notes='nb'
