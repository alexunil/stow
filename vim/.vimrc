set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'davidhalter/jedi-vim'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'dense-analysis/ale'
Plugin 'igorpejic/vim-black'
Plugin 'morhetz/gruvbox'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" Ale Linter
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
let g:ale_python_flake8_options = '--max-line-length=90'
" Black Fixer
autocmd BufWritePost *.py execute ':Black'

set number
set hlsearch
autocmd FileType yaml setlocal et ts=2 ai sw=2 nu sts=0
syntax on

set bg=dark
colorscheme gruvbox

